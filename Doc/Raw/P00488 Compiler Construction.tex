\documentclass{scrartcl}
\usepackage{listings}
\usepackage{bold-extra}
\usepackage{xcolor}
\usepackage{soul}
\usepackage{multirow}
\usepackage{textcomp}
\usepackage[margin=0.8in]{geometry}
\usepackage{graphicx}
\usepackage[titletoc]{appendix}
\graphicspath{ {images/} }
\usepackage{fancyhdr}
\pagestyle{fancy}
\lstset{language=Java,
  showspaces=false,
  showtabs=false,
  breaklines=true,
  showstringspaces=false,
  breakatwhitespace=true,
  escapeinside={(*@}{@*)},
  basicstyle=\ttfamily,
  columns=fullflexible,
}

\begin{document}
\title{Construction of a compiler}
\subtitle{P00488: Compiler Construction}
\date{February 2017}
\author{Girardo Florian}
\fancyhead{}
\fancyfoot{}

\chead{Construction of a compiler}
\cfoot{\thepage}
\maketitle
\newpage
\tableofcontents
\newpage
\section{Introduction}
  This document is an introduction to the creation of a compiler. The compiler will be written in Java and be composed of two major components :
  \begin{itemize}
    \item \textbf{Scanner - } Is going to read strings of code and change them into small identifiers called tokens.
    \item \textbf{Parser - } Is going to read the tokens created by the scanner, interpret them and generate corresponding bytecode.
  \end{itemize}
  The compiler described in this document aim to compile a small programming language derived from Java called \textmu java.

  The scanner should be able to detect any \textmu java keyword (class, else, final, if, new, print, program, read, return, void, while), operator ($ + - * / \% == != <  <= >  >= =  ;  ,  .  (  )  [  ]  {  }$), must ensure that a given number is not causing an overflow and finally be able to scan any given character as well as the three following keywords \textbackslash n \textbackslash t \textbackslash r

  The parser needs to understand the code and know when a new variable, constant or any object is created, modified, called or destroyed and should manage the content of the symbol table accordingly. It should also handle the scopes of the program and store the correct object in the correct scope.
  Eventually the parser should generate bytecode for the \textmu java virtual machine executing the content of the program.
\newpage
\section{Test plan}
  \subsection{Scanner}
  The goal of the scanner is to be able to read a file, detect each character written in the file and be able to distinguish them.
  However this compiler aims to compile \textmu java code meaning that the scanner has to understand only the syntax of this language therefore should return an error when the scanned text contain any non \textmu java character.

  Furthermore it shoulds be able to differentiate the keywords of the scanned code as such so it needs to translate the content of the scanned file into several tokens.
  A token is a scanned bit of code, it could be a number or a letter but can also be any identifier like 'if', 'else'...

  The proper functioning of the scanner will be tested with the help of the given java class named TestScanner available in the TestData directory.
  The purpose of this script is simply to run the scanner on an input file then print some information returned by the scanner.

  For each detected token the information will be the following :
  \begin{itemize}
    \item \textbf The line on which the token has been detected.
    \item \textbf The column on which it start.
    \item \textbf The kind of the token.
    \item \textbf The value of the token.
  \end{itemize}

  The TestScanner script is going to print a formatted version of those information.
  For example when the scanner detect a semi-colon at the line 1 column 1 this is the expected output :
  \begin{lstlisting}[frame=single]
    line 1, col 1: ;
  \end{lstlisting}

  As the scanner return the kind of the token using keywords the test script is able to translate it on the corresponding character.
  For the previous example the scanner would return 'semicolon' as the kind of token.
  Finally when the token is a number or an identifier the value of the token will be printed, for example :
  \begin{lstlisting}[frame=single]
    line 2, col 1: number 5
    line 3, col 5: ident  size
  \end{lstlisting}

  We will test the scanner with three different provided input files :
  \begin{itemize}
    \item \textbf sample.mj : This file contain a syntactically correct \textmu java program, the test should return a formatted line for each token of the program as shown before.
    \item \textbf Eratos.mj : Again this file is a working \textmu java program, therefore the wanted output is the same kind as sample.mj.
    \item \textbf BuggyScannerInput.mj : This time the file is deliberately incorrect. The first part on the file doesn't contain any error therefore should be scanned like the two previous file.

                  On the other hand, it's finishing by six errors that need to be smartly handled by the scanner which imply to read the full document without crashing and report the correct number of errors and their positions.

                  The scanner is expected to return four kind of error messages for the BuggyScannerInput.mj :
                  \begin{itemize}
                    \item "Overflow" for the illegal number.
                    \item "Apostrophe missing" for the illegal character constants with only one apostrophe.
                    \item "Unknown character" for the unknown token.
                    \item "Invalid character" for the three remaining illegal character constants.
                  \end{itemize}
  \end{itemize}
  By testing the scanner with the three files we can be confident in the good working state of the scanner.
  The content of those files are available in the appendices of this document.
  \subsection{Parser}
  The second part of this compiler is the parser which needs to fulfill two different tasks.
  The symbol table is going to store and retrieve all declared names and properties of the \textmu java program being parsed.

  The parser should therefore be able to add a new entry in the table anytime the program declare one with it's properties but also be capable of fetching it whenever the program call it.

  \textmu java like most programming languages uses scopes for variables, our symbol table should then be able to store the corresponding scope for each stored variable.
  This will be imperative when the code is going to be executed by the \textmu java virtual machine.

  Finally once the table management done we want the parser to be able to generate machine instruction, in our case \textmu java virtual machine bytecodes.

  In order to ensure that those features are working properly we are going to test them, like for the scanner, with three different files.
  The java class named TestParser (available in the TestData directory) is going to help us to run the parser on those files.
  \begin{itemize}
    \item \textbf sample.mj : Here again as this program is correct our parser should be able to generate the content of the file and compile it.
    \item \textbf Eratos.mj : Like for sample.mj the parser should be able to parse it and compile it without any error.
    \item \textbf BuggyParserInput.mj : Like for the scanner this file is intentionally incorrect and should produce several errors.
                  We also need the parser to be able to read the entire file and don't stop when encountering an error as we want to inform the user of all errors in only one run.
                  The program contain eight errors that should be returned by the parser. Their are only four different kind of errors and therefore error messages that should be returned :
                  \begin{itemize}
                    \item ""\{" expected" In this case the brace is only an example as we need to handle every missing character of the program.
                    \item "invalid assignment or call" This occurs when the program use an assignment or call symbol that is not recognised by the \textmu java syntax.
                    \item "invalid expression" Should be returned every time an expression in invalid like a function call without attribute.
                    \item "incompatible type in assignment" Should be returned when the the type of the value is not compatible with the type of the variable in an assigment.
                  \end{itemize}
  \end{itemize}
\newpage
\section{Implementation}
  \subsection{Scanner}
  The class Scanner of our compiler will contain six noteworthy functions.

  The first one is the init function that will instantiate a new buffer, set a limit to the size of parsed line to 64, set the buffer location to the first line of the document and call the nextCh function.

  The nextCh function is going to read the next character of the file and detect if it's a classical character, the end of the line or the end of the file.
  This function will therefore increment the current read line when needed or stop the reading process if necessary.

  As those two functions where provided in the scanner skeleton we won't provide any alternative way of writing them, here the final functions :
  \begin{lstlisting}[frame=single]
//----- ch = next input character
private static void nextCh() {
  try {
    ch = (char)in.read(); col++; pos++;
    if (ch == eol) {line++; col = 0;}
    else if (ch == '\uffff') ch = eofCh;
  } catch (IOException e) {
    ch = eofCh;
  }
}

//--------- Initialize scanner
public static void init(Reader r) {
  in = new BufferedReader(r);
  lex = new char[64];
  line = 1; col = 0;
  nextCh();
}
  \end{lstlisting}

  The next important function is the next one :
  \begin{lstlisting}[frame=single]
//---------- Return next input token
public static Token next() {
  while (ch <= ' ')nextCh();
  Token t = new Token();
  t.line = line;
  t.col = col;
  switch(ch){
    case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z':
    case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z':
      readName(t); break;
    case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
      readNumber(t); break;
    case '\'': readCharCon(t); break;
    case '+': nextCh();  t.kind = plus; break;
    case '-': nextCh();  t.kind = minus; break;
    case '*': nextCh();  t.kind = times; break;
    case '/': nextCh();
          if(ch == '/'){
            do nextCh(); while(ch != '\n' && ch != eofCh);
            t = next();
          }
          else t.kind = slash;
          break;
    case '%': nextCh();  t.kind = rem; break;
    case '=': nextCh();
          if (ch == '='){nextCh(); t.kind = eql;}
          else t.kind = assign;
          break;
    case '!': nextCh();
          if (ch == '='){nextCh(); t.kind = neq;}
          else t.kind = none;
          break;
    case '<': nextCh();
              if (ch == '='){nextCh(); t.kind = leq;}
              else t.kind = lss;
              break;
    case '>': nextCh();
                if (ch == '='){nextCh(); t.kind = geq;}
                  else t.kind = gtr;
                  break;
    case ';': nextCh();  t.kind = semicolon; break;
    case ',': nextCh();  t.kind = comma; break;
    case '.': nextCh();  t.kind = period; break;
    case '(': nextCh();  t.kind = lpar; break;
    case ')': nextCh();  t.kind = rpar; break;
    case '[': nextCh();  t.kind = lbrack; break;
    case ']': nextCh();  t.kind = rbrack; break;
    case '{': nextCh();  t.kind = lbrace; break;
    case '}': nextCh();  t.kind = rbrace; break;
    case eofCh: t.kind = eof; break;
    default: nextCh(); System.out.println("Unknown character at line " + t.line + " col " + t.col); t.kind = none; break;
  }
  return t;
}
  \end{lstlisting}

  The purpose of the function is to split the characters of the \textmu java syntax into several categories, by doing so the type of the current token can be set.

  For all letters the function readName is going to be called, for number the function readNumber and for " \textbackslash' " it's the readCharCon function that's called; otherwise the kind to the current token is directly set.
  For these character '=!$<>$' we check if the following character is an equal before setting the type as '==' '!=' '$<$=' and '$>$=' are valid token in \textmu java.

  When the current character is '/' followed by another '/' the scanner detect a comment so ignore all token until the end of the line or the end of the file if the comment is the last line on the program.
  The default value help us to send an error to the user with the position of the invalid character but call the nextCh function in order to continue the scanning process.

  The kind of ch is char which means that despite the desire to write this function with regular expressions it can't be done as in Java the match function is only for string class, even if it was possible it would not be the best way of doing it as we would lose some readability and some execution's time.

  Every time the scanner encounter a letter the following function is called :
  \begin{lstlisting}[frame=single]
private static void readName(Token t) {
  // Parse string of letters or digits.
  int i = 0;
  do {
    if (i < 64){
      lex[i] = ch;
      i++;
      nextCh();
    }
    else {System.out.println("String length is higher than 64 at line " + t.line + " col " + t.col); break;}
  } while ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9'));

  // Check if the string is present in the keyword table.
  t.string = new String(lex,0,i);
  int index = Arrays.binarySearch(key, t.string);

  if (0 <= index){
    // Set the kind of the keyword if it is one.
    t.kind = keyVal[index];
  }
  else{
    t.kind = ident;
  }
}
  \end{lstlisting}

  readName is made of two distinctive parts, first we want the complete word so the do while loop gather it for us.
  In \textmu java variable's names can contain number but not as the first identifier, this is why the while condition include a rule for numbers.
  As we don't want to have a line longer than 64 characters the if statement inside the while loop ensures that the position of the current character on it's line is lower than 64.

  Once we have the complete word we need to check if it's one of the \textmu java keywords.
  After storing the entire word in a variable we are here using the following method :
  \begin{lstlisting}[frame=single]
Arrays.binarySearch(key, t.string);
  \end{lstlisting}

  This function is going to search the array named key, an array contained in the scanner class which include every \textmu java keywords), to check if the word is present.
  This search could has been done by writing a foreach loop that would has compared each element of key with the word.
  However the final way of doing the search is cleaner, shorter and don't affect the execution speed.

  Finally the readName function set the correct type for the token depending of it's presence in the key array.

  When the current token is a number the following function is called :

  \begin{lstlisting}[frame=single]
private static void readNumber(Token t) {
  t.kind = number;

  // Parse string of digits.
  int i = 0;
  do {
    if (i < 64){
      lex[i] = ch;
      i++;
      nextCh();
    }
    else {System.out.println("String length is higher than 64 at line " + t.line + " col " + t.col); break;}
  } while (ch >= '0' && ch <= '9');

  t.string = new String(lex,0,i);

        // Catch overflow.
  try {
    t.val = Integer.valueOf(t.string);
  }catch (NumberFormatException e) {
    System.out.println("Overflow at line " + t.line + " col " + t.col);
  }
}
  \end{lstlisting}

  The first part act like the previous function but only gather numbers instead of letters and numbers.
  We don't need to check for any keyword nonetheless we want to store the value of the token, before doing it we need to check if the value is correct.

  Lastly the following function is called whenever the current token is \textbackslash ' :
  \begin{lstlisting}[frame=single]
private static void readCharCon(Token t){
  int i = 0;
  t.kind = charCon;
  do {
    if (i < 64){
      lex[i] = ch; i++; nextCh();
    }
    else {System.out.println("String length is higher than 64 at line " + t.line + " col " + t.col); break;}
  }while(ch != '\'' && ch != '\n' && ch != eofCh);

  // Adding the second apostrophe in lex.
  // If the second apostrophe is missing return a custom error.
  if (ch == '\''){lex[i] = ch; i++; nextCh();}
  else {System.out.println("Apostrophe missing at line " + t.line + " col " + t.col);}

  // Parsing 'x' kind of string.
  if (i == 3 && ((lex[1] >= 'A' && lex[1] <= 'Z') || (lex[1] >= 'a' && lex[1] <= 'z') || (lex[1] >= '0' && lex[1] <= '9'))){
    t.val = lex[1];
  }
  // Parsing '\x' kind of strings.
  else if(i == 4 && lex[1] == '\\'){
    if (lex[2] == 'r'){t.val = '\r';}
    else if (lex[2] == 'n'){t.val = '\n';}
    else if (lex[2] == 't'){t.val = '\t';}
    // The letter after the backslash must be r, n or t otherwise it must return an error.
    else {System.out.println("Invalid number of characters after \\ at line " + t.line + " col " + t.col);}
  }
  // All other cases must return an error.
  else{System.out.println("Invalid character at line " + t.line + " col " + t.col);}
}
  \end{lstlisting}

  This function cover the scanning of the content of a character or a backslash followed by a character, therefore it needs to grab every character until the next apostrophe. Nonetheless we need to ensure that the user didn't forget to write the second apostrophe this is why the while loop stop at the first apostrophe, end of line or end of file character encountered.

  Before processing the content that the function just parsed the following bit of code check if the second apostrophe is present otherwise return an error :
  \begin{lstlisting}[frame=single]
if (ch == ’\’’){lex[i] = ch; i++; nextCh();}
else {System.out.println("Apostrophe missing at line " + t.line + " col " + t.col);}
  \end{lstlisting}

Finally this function need to scanning the two possible scenario, the first one will be handling a simple char, this is done by the following bit of code :
\begin{lstlisting}[frame=single]
if (i == 3 &&
   ((lex[1] >= ’A’ && lex[1] <= ’Z’) ||
   (lex[1] >= ’a’ && lex[1] <=’z’) ||
   (lex[1] >= ’0’ && lex[1] <= ’9’))){
  t.val = lex[1];
}
\end{lstlisting}

The variable i needs to be of size 3 as we incremented it for the first apostrophe, the char and the second apostrophe.
The second scenario is a char preceded by an backslash, but only 3 letters are permitted in this case : r,n and t :
\begin{itemize}
  \item \textbackslash n is the symbol for a new line
  \item \textbackslash t is the symbol for a new tab
  \item \textbackslash r is the symbol for return
\end{itemize}
All other char needs to return an error, this control is done by the following bit :
\begin{lstlisting}[frame=single]
  else if(i == 4 && lex[1] == '\\'){
    if (lex[2] == 'r'){t.val = '\r';}
    else if (lex[2] == 'n'){t.val = '\n';}
    else if (lex[2] == 't'){t.val = '\t';}
    // The letter after the backslash must be r, n or t otherwise it must return an error.
    else {System.out.println("Invalid number of characters after \\ at line " + t.line + " col " + t.col);}
  }
\end{lstlisting}
This time the variable i needs to be of size 4 as it as been incremented for the backslash.


\subsection{Parser}
\subsubsection{Symbol Table Handling}
As said before the first role of the parser is to be able to store and retrieve all declared names and their properties of the parsed code source.
In order to carry out this task we are going to use the Tab class.
Beside the init method this class own several methods acting on three different way with the table handling :
\begin{itemize}
  \item Interact with the content of the table
  \item Dumping the content of the table
  \item Managing scopes
\end{itemize}
Here again we are not going to explain design choice of the init and dumping methods as they were provided :

\begin{lstlisting}[frame=single]
//-------------- initialization of the symbol table ------------

public static void init() {  // build the universe
  Obj o;
  curScope = new Scope();
  curScope.outer = null;
  curLevel = -1;

  // create predeclared types
  intType = new Struct(Struct.Int);
  charType = new Struct(Struct.Char);
  nullType = new Struct(Struct.Class);
  noType = new Struct(Struct.None);
  noObj = new Obj(Obj.Var, "???", noType);

  // create predeclared objects
  insert(Obj.Type, "int", intType);
  insert(Obj.Type, "char", charType);
  insert(Obj.Con, "null", nullType);
  chrObj = insert(Obj.Meth, "chr", charType);
  chrObj.locals = new Obj(Obj.Var, "i", intType);
  chrObj.nPars = 1;
  ordObj = insert(Obj.Meth, "ord", intType);
  ordObj.locals = new Obj(Obj.Var, "ch", charType);
  ordObj.nPars = 1;
  lenObj = insert(Obj.Meth, "len", intType);
  lenObj.locals = new Obj(Obj.Var, "a", new Struct(Struct.Arr, noType));
  lenObj.nPars = 1;
}
\end{lstlisting}
\begin{lstlisting}[frame=single]
//---------------- methods for dumping the symbol table --------------

public static void dumpStruct(Struct type) {
  String kind;
  switch (type.kind) {
    case Struct.Int:  kind = "Int  "; break;
    case Struct.Char: kind = "Char "; break;
    case Struct.Arr:  kind = "Arr  "; break;
    case Struct.Class:kind = "Class"; break;
    default: kind = "None";
  }
  System.out.print(kind+" ");
  if (type.kind == Struct.Arr) {
    System.out.print(type.nFields + " (");
    dumpStruct(type.elemType);
    System.out.print(")");
  }
  if (type.kind == Struct.Class) {
    System.out.println(type.nFields + "<<");
    for (Obj o = type.fields; o != null; o = o.next) dumpObj(o);
    System.out.print(">>");
  }
}

public static void dumpObj(Obj o) {
  String kind;
  switch (o.kind) {
    case Obj.Con:  kind = "Con "; break;
    case Obj.Var:  kind = "Var "; break;
    case Obj.Type: kind = "Type"; break;
    case Obj.Meth: kind = "Meth"; break;
    default: kind = "None";
  }
  System.out.print(kind+" "+o.name+" "+o.val+" "+o.adr+" "+o.level+" "+o.nPars+" (");
  dumpStruct(o.type);
  System.out.println(")");
}

public static void dumpScope(Obj head) {
  System.out.println("--------------");
  for (Obj o = head; o != null; o = o.next) dumpObj(o);
  for (Obj o = head; o != null; o = o.next)
    if (o.kind == Obj.Meth || o.kind == Obj.Prog) dumpScope(o.locals);
}
\end{lstlisting}

First of all we need to be able to manage the scopes of the \textmu java program. To help us the following class is provided :
\begin{lstlisting}[frame=single]
public class Scope {
	public Scope outer;		// to outer scope
	public Obj   locals;	// to local variables of this scope
	public int   nVars;     // number of variables in this scope
}
\end{lstlisting}

When handling the content of the array our class tab should be able to create a inner scope or to search in all outer scopes, the function openScope is going to create a new scope and store the current scope as the outer value of the new scope creating a recursive link between all scopes :
\begin{lstlisting}[frame=single]
public static void openScope() {
  Scope s = new Scope();
  s.outer = curScope;
  curScope = s;
  curLevel++;
}
\end{lstlisting}

The value of the current level is going to be useful when storing variables into the array.
The function closeScope is going to change the currentScope by it's outer value and decrements the value of the current level :
\begin{lstlisting}[frame=single]
public static void closeScope() {
  curScope = curScope.outer;
  curLevel--;
}
\end{lstlisting}

In order to interact with the content of the symbol table this class has three methods, the first one aim to add any content in the table.
This function should be able to insert four kinds of different objects with their specific information :
\begin{center}
  \begin{tabular}{| c | c |}
  \hline
  \textbf{Object kind} & \textbf{Information needed}\\
  \hline
  All & Name, Type structure, object kind, pointer to the next object \\
  \hline
  Constants & Value \\
  \hline
  Variables & Address, Declaration level \\
  \hline
  Types & - \\
  \hline
  Methods & Address, number of parameters, parameters \\
  \hline
  \end{tabular}
\end{center}

Types are composed by primitives types (like integer or char), arrays and classes.
The insert function could be written like this :
\begin{lstlisting}[frame=single]
// Create a new object with the given kind, name and type
// and insert it into the top scope.
public static Obj insert(int kind, String name, Struct type) {
  Obj obj = new Obj(kind, name, type);
  if (kind == Obj.Var) {
    obj.adr = curScope.nVars; curScope.nVars++;
    obj.level = curLevel;
  }
  //--- append object node
  Obj p = curScope.locals, last = null;
  while (p != null) {
    if (p.name.equals(name)) error(name + " declared twice");
    last = p; p = p.next;
  }
  if (last == null) curScope.locals = obj; else last.next = obj;
  return obj;
}
\end{lstlisting}

In order to insert an object the function is going to instantiate an Obj object which was also provided :

\begin{lstlisting}[frame=single]
  public class Obj {
  	public static final int // object kinds
  		Con  = 0,
  		Var  = 1,
  		Type = 2,
  		Meth = 3,
  		Prog = 4;
  	public int    kind;		// Con, Var, Type, Meth, Prog
  	public String name;		// object name
  	public Struct type;	 	// object type
  	public int    val;    // Con: value
  	public int    adr;    // Var, Math: address
  	public int    level;  // Var: declaration level
  	public int    nPars;  // Meth: number of parameters
  	public Obj    locals; // Meth: parameters and local objects
  	public Obj    next;		// next local object in this scope

  	public Obj(int kind, String name, Struct type) {
  		this.kind = kind; this.name = name; this.type = type;
  	}
  }
\end{lstlisting}

Once this Obj created and it's kind, name and type set we need to check if the currentObj is a variable as we want to store it in the correct scope.

This following bit of code set the correct address and level of declaration of the Obj if it's kind is variable for the current scope :
\begin{lstlisting}[frame=single]
if (kind == Obj.Var) {
  obj.adr = curScope.nVars; curScope.nVars++;
  obj.level = curLevel;
}
\end{lstlisting}

\paragraph{}Now that we can add new object in the symbol table we want to be able to retrieve them and their properties. To do it we need to think about the scopes, when the user call an object we are going to check the current scope then the one above if we are unable to find it until we find it or until we searched in every scopes.
Then inside of each scope we are going to check the name of every stored objects an compare it with the name of the called object.

This is the function find :
\begin{lstlisting}[frame=single]
// Retrieve the object with the given name from the top scope
public static Obj find(String name) {
  for (Scope s = curScope; s != null; s = s.outer)
    for (Obj p = s.locals; p != null; p = p.next)
      if (p.name.equals(name)) return p;
  error(name + " is undeclared");
  return noObj;
}
\end{lstlisting}

If the current object exist the function return it otherwise it return an error.
Now that we can retrieve object we want to be able to retrieve a field, this time we don't need to take the scope into account as we want to research only on the current one.
The function findField is going to search among all objects in the current field with the same type as the one given in as attribute to see if the object is declared :
\begin{lstlisting}[frame=single]
// Retrieve a class field with the given name from the fields of "type"
public static Obj findField(String name, Struct type) {
  for (Obj f = type.fields; f != null; f = f.next)
    if (f.name.equals(name)) return f;
  error(name + " is undeclared");
  return noObj;
}
\end{lstlisting}

\subsubsection{Code Generation}
Now that our compiler is able to scan the content of a given program and manage the symbol table it need to transform the code into bytecode for the \textmu java virtual machine.
This part of the compiler is done by the parser class.
As this class contain more than 700 lines it would be counterproductive to copy and past the content in this document, as it can be read in the code directory.
This class contain a method for each elements of the \textmu java syntax :
\begin{itemize}
  \item Program
  \item ConstDecl
  \item VarDecl
  \item ClassDecl
  \item MethodDecl
  \item FormPars
  \item Type
  \item Block
  \item Statement
  \item ActPars
  \item Condition
  \item Relop
  \item Expr
  \item Term
  \item Factor
  \item Designator
  \item Addop
  \item Mulop
\end{itemize}
The syntax of those elements can be read in the MicroJava Quick Reference document.
For each function a semantic processing and attributed grammars was provided.

\newpage
\section{Testing}
  \subsection{Scanner}
  \subsubsection{BuggyScanner}
  This is the output of scanner when using the file BuggyScanner.mj as input :
  \begin{center}
    \includegraphics[scale=0.35]{BuggyParser}\\
    \label{Output of the scanner for the file BuggyScanner}
  \end{center}
  The parser went through the entire file regarding of any encountered errors and print en error message for all of them beside the invalid character which as a none type.

  \subsubsection{Sample}
  This is the output of scanner when using the file sample.mj as input :
  \begin{center}
    \includegraphics[scale=0.35]{ParserSample1}\\
    \label{Output of the scanner for the file sample part 1}
  \end{center}
  \begin{center}
    \includegraphics[scale=0.35]{ParserSample2}\\
    \label{Output of the scanner for the file sample part 2}
  \end{center}

  \subsubsection{Eratos}
  This is the output of scanner when using the file Eratos.mj as input :
  \begin{center}
    \includegraphics[scale=0.35]{ParserEratos1}\\
    \label{Output of the scanner for the file Eratos part 1}
  \end{center}
  \begin{center}
    \includegraphics[scale=0.35]{ParserEratos2}\\
    \label{Output of the scanner for the file Eratos part 2}
  \end{center}

  \subsection{Parser}
  The current parser is able to handle the syntax table and scope but because of some errors that I was not able to solve the code generation part of the scanner is not working, this part is therefore irrelevant.

\newpage
\section{Summary}
Nowadays with the rise of more high level languages and tools to helps the developer in the development process creating a compiler could appears a complex unnecessary project; nevertheless after following this module and working on this coursework it seems to me that producing this kind of project is important even essential for a developer to fully understand how those tools are working.
By creating a compiler for a \textmu java I was able to understand the concepts behind it therefore the decisions leading to the design of the commons programming languages despite their richer syntax.

The parsing part even help me to fathomed the design of interpreted languages and helped me thinking of new ways to parse all sorts of syntax.
Creating a compiler can only brings expertise and knowledge to the developer.

Unfortunately in spit of searching my mistakes I was not able to fully finish this compiler but I'm going to rewrite it entirely without time's constraint in order to not staying on this failure.

\newpage
\begin{appendices}
\section{sample.mj}
\begin{lstlisting}[frame=single]
  program P
    final int size = 10;
    class Table {
      int[] pos;
      int[] neg;
    }
    Table val;
  {
    void main()
      int x, i;
    { //---------- Initialize val
      val = new Table;
      val.pos = new int[size]; val.neg = new int[size];
      i = 0;
      while (i < size) {
        val.pos[i] = 0; val.neg[i] = 0;
        i = i + 1;
      }
    //---------- Read values
      read(x);
      while (x != 0) {
        if (x >= 0) {
          val.pos[x] = val.pos[x] + 1;
        } else if (x < 0) {
          val.neg[-x] = val.neg[-x] + 1;
        }
        read(x);
      }
    }
  }
\end{lstlisting}
\section{Eratos.mj}
\begin{lstlisting}[frame=single]
  program Eratos

    char[] sieve;
    int max;    // maximum prime to be found
    int npp;    // numbers per page

  {
    void put(int x)
    {
      if (npp == 10) {print(chr(13)); print(chr(10)); npp = 0;}
      print(x, 5);
      npp = npp + 1;
    }

    void found(int x)
      int i;
    {
      put(x);
      i = x;
      while (i <= max) {sieve[i] = 'o'; i = i + x;}
    }

    void main()
      int i, ready;
    {
      read(max);
      npp = 0;
      sieve = new char[max+1];
      i = 0;
      while (i <= max) {sieve[i] = 'x'; i = i + 1;}
      i = 2;
      while (i <= max) {
        found(i);
        ready = 0;
        while(ready == 0) {
          if (i > max) ready = 1;
          else if (sieve[i] == 'x') ready = 1;
          else i = i + 1;
        }
      }
    }

  }
\end{lstlisting}
\section{BuggyScannerInput.mj}
\begin{lstlisting}[frame=single]
  // Test input for the MicroJava scanner
  //*****************************************************
  // invoke as: java MJ.TestScanner BuggyScannerInput.mj
  //*****************************************************

  // identifiers
  x
  Just4Fun

  // numbers
  0
  00123
  123

  // character constants
  'x'
  '\r'
  '\n'
  '\t'

  // operators
  +  // plus
  -  // minus
  *  // times
  /  // slash
  %  // rem
  == // eql
  != // neq
  <  // lss
  <= // leq
  >  // gtr
  >= // geq
  =  // assign
  ;  // semicolon
  ,  // comma
  .  // period
  (  // lpar
  )  // rpar
  [  // lbrack
  ]  // rbrack
  {  // lbrace
  }  // rbrace

  // keywords
  class
  else
  final
  if
  new
  print
  program
  read
  return
  void
  while

  // illegal number
  9999999999

  // illegal character constants
  'abc'
  '\rx'
  ''
  'x

  // unknown token
  $
\end{lstlisting}
\section{BuggyScannerInput.mj}
\begin{lstlisting}[frame=single]
  // Test input for the MicroJava parser
  //***************************************************
  // invoke as: java MJ.TestParser BuggyParserInput.mj
  //***************************************************

  program P
    final int size = 10;
    class Table
  //------------| "{" expected
      int[] pos;
      int[] neg
  //-----------| ";" expected
    }
    Table val;
  {
    void main()
      int x, i;
    { val := new Table;
  //------| invalid assignment or call
      val.pos = new int[size); val.neg = new int[size];
  //------------------------| "]" expected
      i = 0;
      while i < size {
  //--------| "(" expected
  //----------------| ")" expected
        val.pos[i] = 0; val.neg[i] = 0;
        i = i + 1;
      }
      x = read();
  //------| invalid expression
      while (x != 0) {
        if (x >= 0) {
          val.pos[x] = val pos[x] + 1;
  //----------------------| incompatible type in assigment
        } else if (x < 0) {
          val.neg[-x] = val.neg[-x] + 1;
        }
        read(x);
      }
    }
  }
\end{lstlisting}
\end{appendices}

\end{document}
