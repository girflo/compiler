/*  MicroJava Parser (HM 06-12-28)
    ================
*/
package MJ;

import java.beans.Expression;
import java.util.*;
import java.util.concurrent.locks.Condition;

import MJ.SymTab.*;
import MJ.CodeGen.*;

public class Parser {
	private static final int  // token codes
		none      = 0,
		ident     = 1,
		number    = 2,
		charCon   = 3,
		plus      = 4,
		minus     = 5,
		times     = 6,
		slash     = 7,
		rem       = 8,
		eql       = 9,
		neq       = 10,
		lss       = 11,
		leq       = 12,
		gtr       = 13,
		geq       = 14,
		assign    = 15,
		semicolon = 16,
		comma     = 17,
		period    = 18,
		lpar      = 19,
		rpar      = 20,
		lbrack    = 21,
		rbrack    = 22,
		lbrace    = 23,
		rbrace    = 24,
		class_    = 25,
		else_     = 26,
		final_    = 27,
		if_       = 28,
		new_      = 29,
		print_    = 30,
		program_  = 31,
		read_     = 32,
		return_   = 33,
		void_     = 34,
		while_    = 35,
		eof       = 36;
	private static final String[] name = { // token names for error messages
		"none", "identifier", "number", "char constant", "+", "-", "*", "/", "%",
		"==", "!=", "<", "<=", ">", ">=", "=", ";", ",", ".", "(", ")",
		"[", "]", "{", "}", "class", "else", "final", "if", "new", "print",
		"program", "read", "return", "void", "while", "eof"
		};

	private static Token t;			// current token (recently recognized)
	private static Token la;		// lookahead token
	private static int sym;			// always contains la.kind
	public  static int errors;  // error counter
	private static int errDist;	// no. of correctly recognized tokens since last error

	private static BitSet exprStart, statStart, statSeqFollow, syncStat, declStart, declFollow;

	private static Obj currentMethod;

	//------------------- auxiliary methods ----------------------
	private static void scan() {
		t = la;
		la = Scanner.next();
		sym = la.kind;
		errDist++;
		/*
		System.out.print("line " + la.line + ", col " + la.col + ": " + name[sym]);
		if (sym == ident) System.out.print(" (" + la.string + ")");
		if (sym == number || sym == charCon) System.out.print(" (" + la.val + ")");
		System.out.println();*/
	}

	private static void check(int expected) {
		if (sym == expected) scan();
		else error(name[expected] + " expected");
	}

	public static void error(String msg) { // syntactic error at token la
		if (errDist >= 3) {
			System.out.println("-- line " + la.line + " col " + la.col + ": " + msg);
			errors++;
		}
		errDist = 0;
	}

	//-------------- parsing methods (in alphabetical order) -----------------

	// ActPars = "(" [ Expr {"," Expr} ] ")".
	private static void ActPars(Operand m) {
		Operand ap;
		check(lpar);
		if (m.kind != Operand.Meth) { error("not a method"); m.obj = Tab.noObj; }
		int aPars = 0;
		int fPars = m.obj.nPars;
		Obj fp = m.obj.locals;

		if (exprStart.get(sym)){
			ap = Expr();
			Code.load(ap);
			aPars++;
			if (fp != null) {
				if (!ap.type.assignableTo(fp.type)) error("parameter type mismatch");
				fp = fp.next;
			}
			for(;;){
				if(sym == comma){
					scan();
					ap = Expr();
					Code.load(ap);
					aPars++;
					if (fp != null) {
						if (!ap.type.assignableTo(fp.type)) error("parameter type mismatch");
						fp = fp.next;
					}
				}
				else break;
			}
		}
		if (aPars > fPars) error("too many actual parameter");
		else if (aPars < fPars) error("too few actual parameters");
		check(rpar);
	}

	// Addop =  "+" | "-".
	private static int Addop() {
		// set + as the default symbol
		int op = Code.add;
		if (sym == plus){scan();}
		else if (sym == minus){op = Code.sub; scan();}
		else{error("expected math symbol");}
		return op;
	}

	// Block = "{" {Statement} "}".
	private static void Block() {
		check(lbrace);
		while(sym != rbrace && sym != eof){
			Statement();
		}
		check(rbrace);
	}

	// ClassDecl =  "class" ident "{" {VarDecl} "}".
	private static void ClassDecl() {
		check(class_);
		// creating the structure of the class
		Struct type = new Struct(Struct.Class);
		check(ident);
		Tab.insert(Obj.Type, t.string, type);

		// create the scope for the class
		Tab.openScope();
		check(lbrace);
		for(;;){
			// VarDecl =  Type ident {"," ident } ";".
			if (sym == ident){VarDecl();}
			else break;
		}
		// adding the variable of the new class in the current scope
		type.fields = Tab.curScope.locals;
		// adding the number of variables in the current scope
		type.nFields = Tab.curScope.nVars;
		// close the class' scope
		Tab.dumpScope(Tab.curScope.locals);
		Tab.closeScope();
		check(rbrace);
	}

	// Condition = Expr Relop Expr.
	private static int Condition() {
		// collect the first expression to compare
		Operand x = Expr();

		// load the first expression in the buffer
		Code.load(x);

		// get the correct operand
		int op = Relop();

		// collect the second expression to compare
		Operand y = Expr();

		// load the second expression in the buffer
		Code.load(y);

		if (!x.type.compatibleWith(y.type)) error("type mismatch");
		if (x.type.isRefType() && op != Code.eq && op != Code.ne) error("invalid compare");
		return op;
	}

	// ConstDecl =  "final" Type ident "=" (number | charConst) ";".
	private static void ConstDecl() {
		check(final_);
		// get the structure of the type of the constant
		Struct type = Type();
		check(ident);
		// insert the constant in the symbol table
		Obj obj = Tab.insert(Obj.Con, t.string, type);
		check(assign);
		if ((sym == number && obj.type == Tab.intType) || (sym == charCon && obj.type == Tab.charType)){
			scan();
			// set the value of the constant
			obj.val = t.val;
		}
		else{error("expected number or character");}
		check(semicolon);
	}

	// Designator =  ident {"." ident | "[" Expr "]"}.
	private static Operand Designator() {
		check(ident);
		// search for the first ident in the symbol table
		Obj obj = Tab.find(t.string);

		// store the type of the ident
		Struct type = obj.type;

		// create a new operand with obj as model
		Operand x = new Operand(obj);

		for(;;){
			// test if the given designator is a class
			if (sym == period){
				scan();

				if(type.kind == Struct.Class || obj.kind == Obj.Var){
					check(ident);

					// load the operand in the stack
					Code.load(x);

					// search for correct field for the type class in the symbol table
					Obj fld = Tab.findField(t.string, type);

					// store parameters of the field found in the symbol table in the operand
					x.kind = Operand.Fld; // create a field kind operand
					x.adr = fld.adr;
					x.type = fld.type;
				}
				else{error(t.string + " is not an object");}
			}
			// test if the given designator is an array
			else if (sym == lbrack){
				scan();

				// load the operand in the stack
				Code.load(x);
				if(type.kind == Struct.Arr){
					// collect the expression between the brackets
					Operand y = Expr();
					if (y.type.kind != Struct.Int){
						error("index must be of type int");
					}
					else {
						// load the operand in the stack
						Code.load(y);
						x.kind = Operand.Elem; // create a element kind operand
						x.type = x.type.elemType;
					}
				}
				else {
					error(name + " is not an array");
				}
				check(rbrack);
			}
			else break;
		}

		return x;
	}

	// Expr = ["-"] Term {Addop Term}.
	private static Operand Expr() {
		Operand x;
		if (sym == minus){
			scan();
			// collect the first term
			x = Term();

			if (x.type != Tab.intType) {error("operand must be of type int");}

			// if x is a constant change directly the value of x by the value on the minus constant
			if (x.kind == Operand.Con) x.val = -x.val;
			// if x is not a constant use the fonction neg to change the content of x
			else {Code.load(x);Code.put(Code.neg);}
		} else {
			x = Term();
			if (x.type != Tab.intType) {error("operand must be of type int");}
		}

		for(;;){
			if(sym == minus || sym == plus){
				int op = Addop();

				// load the first operand in the stack
				Code.load(x);

				// collect the next term
				Operand y = Term();

				// load the next operand in the stack
				Code.load(y);

				if (x.type != Tab.intType || y.type != Tab.intType) error("operands must be of type int");

				Code.put(op);
			}
			else break;
		}
		return x;
	}

	/*
	 * Factor = Designator [ActPars]
     * | number
     * | charConst
     * | "new" ident ["[" Expr "]"]
     * | "(" Expr ")".
	 */
	private static Operand Factor() {
		Operand x = null;
		// Designator [ActPars]
		if (sym == ident){

			Operand m = Designator();
			// Check for any ActPars
			if (sym == lpar){
				ActPars(m);
				if (m.type == Tab.noType) error("procedure called as a function");
				if (m.obj == Tab.ordObj || m.obj == Tab.chrObj) ;
				else if (m.obj == Tab.lenObj)
					// Add an array in the buffer
					Code.put(Code.arraylength);
				else {
					// Add a call in the buffer
					Code.put(Code.call);
					//  Set the address of the call is the buffer
					Code.put2(m.adr);
				}
				x = m; x.kind = Operand.Stack;
			}
			else{x=m;}
		}
		// number
		else if (sym == number){
			// create a new operand with int as model
			x = new Operand(t.val);
			scan();
		}
		// charConst
		else if (sym == charCon){
			// create a new operand with char as model
			x = new Operand(t.val);
			x.type = Tab.charType;
			scan();
		}
		// "new" ident ["[" Expr "]"]
		else if (sym == new_){
			scan();
			check(ident);
			// search for the first ident in the symbol table
			Obj obj = Tab.find(t.string);
			Struct type = obj.type;

			// if the new ident is an array
			if (sym == lbrack){
				scan();
				// Check if obj is of kind Type
				if (obj.kind != Obj.Type) error("type expected");
				x = Expr();
				if (x.type != Tab.intType) error("array size must be of type int");
				Code.load(x);

				// Create and add a new array in the buffer
				Code.put(Code.newarray);
				// If type is a char we just need
				if (type == Tab.charType) Code.put(0);
				else Code.put(1);
				type = new Struct(Struct.Arr, type);
				check(rbrack);
			}
			// if the new ident is a class
			else {
				if (obj.kind != Obj.Type || type.kind != Struct.Class)	error("class type expected");

				// Add the keyword new and the number of filed to the buffer
				Code.put(Code.new_);
				Code.put2(type.nFields);
			}
			x = new Operand();
			x.kind = Operand.Stack;
			x.type = type;
		}
		// "(" Expr ")"
		else if (sym == lpar){
			scan();
			x = Expr();
			check(rpar);
		}
		else {
			error("invalid expression");
		}
		return x;
	}

	// FormPars =  Type ident  {"," Type ident}.
	private static int FormPars() {
		int i = 1;
		// get the structure of the type of the first parameter
		Struct mandatoryType = Type();
		check(ident);
		// insert the first parameter in the symbol table
		Tab.insert(Obj.Var, t.string, mandatoryType);
		for(;;){
			if (sym == comma){
				scan();
				// get the structure of the type of the other parameters
				Struct optionalType = Type();
				check(ident);
				Tab.insert(Obj.Var, t.string, optionalType);
				i++ ;
			}
			else break;
		}
		return i;
	}

	// MethodDecl =  (Type | "void") ident "(" [FormPars] ")" {VarDecl} Block.
	private static void MethodDecl() {
		// create a structure of notype for void methods
		Struct type = Tab.noType;
		if (sym == void_){scan();}
		else if (sym == ident){
			// change type with the structure of the type of the method
			type = Type();
		}
		else {
			error("missing type of the method");
		}
		check(ident);
		// insert the method in the symbol table
		currentMethod = Tab.insert(Obj.Meth, t.string, type);

		// create the scope for the method
		Tab.openScope();
		check(lpar);
		// FormPars =  Type ident  {"," Type ident}.
		if (sym == ident){
			currentMethod.nPars = FormPars();
		}
		if (t.string.equals("main")) {
			Code.mainPc = Code.pc;
			if (currentMethod.type != Tab.noType) error("method main must be void");
			if (currentMethod.nPars != 0) error("main must not have parameters");
		}
		check(rpar);

		for(;;){
			// VarDecl =  Type ident {"," ident } ";"."
			if (sym == ident){VarDecl();}
			else break;
		}

		currentMethod.locals = Tab.curScope.locals;
		currentMethod.adr = Code.pc;
		Code.put(Code.enter);
		Code.put(currentMethod.nPars);
		Code.put(Tab.curScope.nVars);

		Block();

		if (currentMethod.type == Tab.noType) {
			Code.put(Code.exit); Code.put(Code.return_);
		} else { // end of function reached without a return statement
			Code.put(Code.trap); Code.put(1);
		}

		// close the method's scope
		Tab.dumpScope(Tab.curScope.locals);
		Tab.closeScope();
	}

	// Mulop = "*" | "/" | "%".
	private static int Mulop() {
		// set * as the default symbol
		int op = Code.mul;
		if (sym == times)scan();
		else if (sym == slash){op = Code.div; scan();}
		else if (sym == rem){op = Code.rem; scan();}
		else{error("expected math symbol");}
		return op;
	}

	// Program = "program" ident {ConstDecl | ClassDecl | VarDecl} '{' {MethodDecl} '}'.
	private static void Program() {
		check(program_);
		check(ident);
		// create the scope for the program
		Tab.openScope();
		while(sym != eof && sym != lbrace){
			if(sym != final_ && sym != class_ && sym != ident){
				error("invalid start of declaration");
				do scan(); while(sym != final_ && sym != class_ && sym != ident && sym != eof && sym != lbrace);
				errDist = 0;
			}
			if(sym == final_){
				ConstDecl();
			}else if (sym == ident){
				VarDecl();
			}else if (sym == class_){
				ClassDecl();
			}
		}
		check(lbrace);
		// MethodDecl = (Type | "void") ident "(" [FormPars] ")" {VarDecl} Block.
		for(;;){
			if (sym == void_ || sym == ident){MethodDecl();}
			else break;
		}
		check(rbrace);
		// store the numbers of variables as code datasize
		Code.dataSize = Tab.curScope.nVars;
		// close the program's scope
		Tab.dumpScope(Tab.curScope.locals);
		Tab.closeScope();
	}


	// Relop = "==" | "!=" | ">" | ">=" | "<" | "<=".
	private static int Relop() {
		// set the default operand to =
		int op = Code.eq;
		if (sym == eql){scan();}
		else if (sym == neq){scan();op = Code.ne;}
		else if (sym == gtr){scan();op = Code.gt;}
		else if (sym == geq){scan();op = Code.ge;}
		else if (sym == lss){scan();op = Code.lt;}
		else if (sym == leq){scan();op = Code.le;}
		else {error("invalid comparison symbol");}
		return op;
	}


	/*
	 * Statement
	 * =  Designator ("=" Expr | ActPars) ";"
	 * |  "if" "(" Condition ")" Statement ["else" Statement]
	 * |  "while" "(" Condition ")" Statement
	 * |  "return" [Expr] ";"
	 * |  "read" "(" Designator ")" ";"
	 * |  "print" "(" Expr ["," number] ")" ";"
	 * |  Block
	 * |  ";".
	 */
	private static void Statement(){
		int op, adr, adr2, top;
		if (!statStart.get(sym)) {
			error("invalid start of statement");
			while (!syncStat.get(sym)) scan();
			errDist = 0;
		}
		if (sym == ident) {
			// Designator ("=" Expr | ActPars) ";"
			Operand x = Designator();
			if (sym == assign){scan(); Expr();}
			// ActPars = "(" [ Expr {"," Expr} ] ")".
			else if (sym == lpar){
				ActPars(x);
				Code.put(Code.call);
				Code.put2(x.adr);
				if (x.type != Tab.noType) Code.put(Code.pop);
			}
			else {
				error("invalid assignment or call");
			}
			check(semicolon);
		}
		else if (sym == if_){
			// "if" "(" Condition ")" Statement ["else" Statement]
			scan();
			check(lpar);
			// Creation of the condition
			op = Condition();
			// Add the jump for when the condition is false in the buffer
			Code.putFalseJump(op, 0);
			adr = Code.pc - 2;
			check(rpar);
			Statement();
			// For when there is an else statement
			if (sym == else_){
				scan();
				// Add the position where the false jump need to jump
				Code.putJump(0);
				adr2 = Code.pc - 2;
				Code.fixup(adr);
				Statement();
				Code.fixup(adr2);
				Code.fixup(adr);
			}
		}
		else if (sym == while_){
			// "while" "(" Condition ")" Statement
			scan();
			// Store the position of the buffer
			top = Code.pc;

			check(lpar);
				op = Condition();
				// Create the jump for when the condition is false
				Code.putFalseJump(op, 0);
				adr = Code.pc - 2;
			check(rpar);
			Statement();
			// Return to the position of the buffer store in the top, therefore before the while loop
			Code.putJump(top);
			// Position where the false jump need to jump
			Code.fixup(adr);
		}
		else if (sym == return_){
			// "return" [Expr] ";"
			scan();
			if (exprStart.get(sym)){
				Operand x = Expr();
				if (currentMethod.type == Tab.noType) error("void method must not return a value");
				else if (!x.type.assignableTo(currentMethod.type)) error("type of return value must match method type");
			}
			check(semicolon);
		}
		else if (sym == read_){
			// "read" "(" Designator ")" ";"
			scan();
			check(lpar);
			Designator();
			check(rpar);
			check(semicolon);
		}
		else if (sym == print_){
			// "print" "(" Expr ["," number] ")" ";"
			scan();
			check(lpar);
			Expr();
			if (sym == comma){scan(); check(number);}
			check(rpar);
			check(semicolon);
		}
		else if (sym == lbrace){
			// Block
			Block();
		}
		else if (sym == semicolon){
			scan();
		}
		else{
			error("Invalid character");
		}
	}


	// Term = Factor {Mulop Factor}.
	private static Operand Term() {
		// collect the first factor
		Operand x = Factor();
		for(;;){
			if(sym == times || sym == slash || sym == rem){
				int op = Mulop();

				// load the first factor in the stack
				Code.load(x);

				// collect the next factor
				Operand y = Factor();

				// load the next factor in the stack
				Code.load(y);

				if (x.type != Tab.intType || y.type != Tab.intType)	error("operands must be of type int");
				Code.put(op);
			}
			else break;
		}
		return x;
	}


	// Type =  ident ["[" "]"]
	private static Struct Type() {
		check(ident);
		Obj obj = Tab.find(t.string);
		// check which kind is our name
		if (obj.kind != Obj.Type) error("type name expected");
		Struct type = obj.type;
		// check if the type is an array
		if (sym == lbrack){
			scan();
			check(rbrack);
			// create an array of int
			type = new Struct(Struct.Arr, type);
		}
		return type;
}


	// VarDecl =  Type ident {"," ident } ";".
	private static void VarDecl() {
		// get the structure of the type of the first variable
		Struct type = Type();

		check(ident);

		// insert the variable in the symbol table
		Tab.insert(Obj.Var, t.string, type);
		for(;;){
			if (sym == comma){
				scan();
				// get the structure of the type of the other variables
				check(ident);
				Tab.insert(Obj.Var, t.string, type);
			}
			else break;
		}
		check(semicolon);
	}

	public static void parse() {
		// initialize symbol sets
		BitSet s;
		s = new BitSet(64); exprStart = s;
		s.set(ident); s.set(number); s.set(charCon); s.set(new_); s.set(lpar); s.set(minus);

		s = new BitSet(64); statStart = s;
		s.set(ident); s.set(if_); s.set(while_); s.set(read_);
		s.set(return_); s.set(print_); s.set(lbrace); s.set(semicolon);

		s = new BitSet(64); syncStat = s;
		s.set(eof); s.set(if_); s.set(while_); s.set(read_);
		s.set(return_); s.set(print_); s.set(lbrace); s.set(semicolon);

		s = new BitSet(64); statSeqFollow = s;
		s.set(rbrace); s.set(eof);

		s = new BitSet(64); declStart = s;
		s.set(final_); s.set(ident); s.set(class_);

		s = new BitSet(64); declFollow = s;
		s.set(lbrace); s.set(void_); s.set(eof);

		// initialize tab
		Tab.init();
		Code.init();

		// start parsing
		errors = 0; errDist = 3;
		scan();
		Program();
		if (sym != eof) error("end of file found before end of program");
	}

}
