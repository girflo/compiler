import java.io.*;
class TreeBuilder {
  public static void main (String[] args) {
    String inputFile = args[0];
    Scanner scanner = new Scanner(inputFile);
    Parser parser = new Parser(scanner);
    parser.Parse();
    System.out.println(parser.errors.count + " errors detected");
  }
}
